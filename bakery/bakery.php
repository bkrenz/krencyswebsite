<!DOCTYPE html>
<html>

    <head>

        <title>Krency&#8217s Bakery & Catering</title>
        <link rel="stylesheet" href="css/content.css"/>

    </head>


    <body>

        <?php include "php/header.php"?>

        <center>
            <a href="files/cateringmenu.pdf">
                <div class="download"/>
                    <h2>Download our Catering Menu! PDF</h2>
                </div>
            </a>

            <div class="content">
                <h1>Wedding Cakes</h1>
                <img src="images/weddingcake.png"/>
                <p>Krency&#8217s Bakery has created Washington&#8217s finest wedding cakes since 1959. The thousands of cakes that have walked out of our doors are the highlights of many couples&#8217 wedding celebrations from generation to generation. The bridal cake experience begins with a creative meeting and tasting with Doug. The meeting gives the bride the opportunity to view pictures of his cakes and describe her own dream cake. The bride is encouraged to bring pictures! From here Doug sketches the cake and then he and the staff craft it from scratch for the wedding. All cakes are made specially for each bride and groom.</p>
                <p></p>
                <p> Doug doesn&#8217t stop at cakes either, he also will cater and service the entire wedding. Everything from gourmet food to chocolate fountains!</p>
                <a href="files/weddingcakes.pdf">
                    <div class="download"/>
                        <h2>Download our Wedding Cakes Flyer! PDF</h2>
                    </div>
                </a>
            </div>

            

        </center>


    </body>



</html>