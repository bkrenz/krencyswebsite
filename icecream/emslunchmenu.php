<!DOCTYPE html>
<?php
    include "../scripts/database/database.php";
    $db = new Database();


?>
<html>

    <head>

        <title>Krency's Ice Cream - Lunch</title>
        <link rel="stylesheet" href="/css/content.css"/>
        <link rel="stylesheet" href="/css/icecream.css"/>
        <link rel="stylesheet" href="/css/header.css"/>

    </head>


    <body>

        <div class="header">
            <? $logo = "../images/icecream.png"; include "../common/header.php"; ?>
        </div>

        <center>
        <div class="row">

            <div class="foodgroup">
                <h2>Soup and Salad</h2>
                <table>
                    <tr>
                        <th>Item</th>
                        <th>Small</th>
                        <th>Large</th>
                    </tr>
                    <? echo $db -> formatAsTableRows( " SELECT * FROM icecreamproducts WHERE Category= 'Lunch' AND Subcategory='Soup'", 2 ) ?>
                    <? echo $db -> formatAsTableRows( " SELECT * FROM icecreamproducts WHERE Category= 'Lunch' AND Subcategory='Salad'", 2 ) ?>

                </table>
            </div>

            <div class="foodgroup">
                <h2>Sandwiches</h2>
                <p>All sandwiches include a side.</p>
                <table>
                    <tr>
                        <th>Item</th>
                        <th>Price</th>
                    </tr>
                    <tr>
                        <td>BLT</td>
                        <td>$6.99</td>
                    </tr>
                    <tr>
                        <td>Grilled Cheese</td>
                        <td>$5.99</td>
                    </tr>
                    <tr>
                        <td>Grilled Ham & Cheese</td>
                        <td>$7.59</td>
                    </tr>
                    <tr>
                        <td>Grilled Steak & Cheese</td>
                        <td>$8.29</td>
                    </tr>
                    <tr>
                        <td>Grilled Italian</td>
                        <td>$8.29</td>
                    </tr>
                    <tr>
                        <td>Chicken Sandwich</td>
                        <td>$7.99</td>
                    </tr>
                    <tr>
                        <td>Fish Sandwich</td>
                        <td>$7.99</td>
                    </tr>
                    <tr>
                        <td>Hot Roast Beef with Gravy</td>
                        <td>$7.99</td>
                    </tr>
                    <tr>
                        <td>Hot Dog</td>
                        <td>$5.29</td>
                    </tr>
                    <tr>
                        <td>Footlong Hot Dog</td>
                        <td>$6.29</td>
                    </tr>
                </table>
            </div>

            <div class="foodgroup">
                <h2>Dinners</h2>
                <p>All dinners include a side.</p>
                <table>
                    <tr>
                        <th>Item</th>
                        <th>Price</th>
                    </tr>
                    <tr>
                        <td>Chicken Tenders (4)</td>
                        <td>$8.99</td>
                    </tr>
                    <tr>
                        <td>Wing Dings (8)</td>
                        <td>$9.59</td>
                    </tr>
                </table>
            </div>

            <div class="foodgroup">
                <h2>Burgers</h2>
                <p>All burgers include a side.</p>
                <table>
                    <tr>
                        <th>Item</th>
                        <th>Price</th>
                    </tr>
                    <tr>
                        <td>Hamburger</td>
                        <td>$8.59</td>
                    </tr>
                    <tr>
                        <td>Cheeseburger</td>
                        <td>$8.99</td>
                    </tr>
                    <tr>
                        <td>Bacon Cheeseburger</td>
                        <td>$9.99</td>
                    </tr>
                    <tr>
                        <td>Mushroom Cheeseburger</td>
                        <td>$9.49</td>
                    </tr>
                    <tr>
                        <td>Mike&#8217s Burger (Provolone, Jalapenos)</td>
                        <td>$9.49</td>
                    </tr>
                </table>
            </div>
            </div>
        </center>

        <center>

            <div class="row">
            <div class="foodgroup">
                <h2>Hoagies</h2>
                <table>
                    <tr>
                        <th>Item</th>
                        <th>Price</th>
                    </tr>
                    <tr>
                        <td>Italian</td>
                        <td>$8.59</td>
                    </tr>
                    <tr>
                        <td>Steak and Cheese</td>
                        <td>$8.99</td>
                    </tr>
                </table>
            </div>

            <div class="foodgroup">
                <h2>Pizza Oven</h2>
                <table>
                    <tr>
                        <th>Item</th>
                        <th>Price</th>
                    </tr>
                    <tr>
                        <td>Half Small Pizza (2 Slices)</td>
                        <td>$2.99</td>
                    </tr>
                    <tr>
                        <td>Small Pizza (4 Slices)</td>
                        <td>$5.59</td>
                    </tr>
                    <tr>
                        <td>Large Pizza (8 Slices)</td>
                        <td>$8.99</td>
                    </tr>
                    <tr>
                        <td>Additional Toppings</td>
                        <td>$.60</td>
                    </tr>
                    <tr>
                        <td>Stromboli</td>
                        <td>$8.59</td>
                    </tr>
                </table>
            </div>

            <div class="foodgroup">
                <h2>Sides</h2>
                <table>
                    <tr>
                        <th>Item</th>
                        <th>Price</th>
                    </tr>
                    <tr>
                        <td>French Fries</td>
                        <td>$2.99</td>
                    </tr>
                    <tr>
                        <td>Curly Fries</td>
                        <td>$2.99</td>
                    </tr>
                    <tr>
                        <td>Onion Rings</td>
                        <td>$2.99</td>
                    </tr>
                    <tr>
                        <td>Cheese Sticks</td>
                        <td>$5.99</td>
                    </tr>
                </table>
            </div>

            <div class="foodgroup">
                <h2>Kids Menu</h2>
                <p>Includes fries.</p>
                <table>
                    <tr>
                        <th>Item</th>
                        <th>Price</th>
                    </tr>
                    <tr>
                        <td>Cheese Burger</td>
                        <td>$4.99</td>
                    </tr>
                    <tr>
                        <td>Hot Dog</td>
                        <td>$3.29</td>
                    </tr>
                    <tr>
                        <td>Grilled Cheese</td>
                        <td>$3.99</td>
                    </tr>
                    <tr>
                        <td>Chicken Tenders (2)</td>
                        <td>$4.99</td>
                    </tr>
                </table>
            </div>
            </div>

        </center>


    </body>



</html>