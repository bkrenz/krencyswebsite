<!DOCTYPE html>
<html>

    <head>

        <title>Krency's Ice Cream</title>
        <link rel="stylesheet" href="/css/content.css"/>
        <link rel="stylesheet" href="/css/icecream.css"/>
        <link rel="stylesheet" href="/css/header.css"/>

    </head>


    <body>

        <div class="header">
            <? $logo = "/images/bakery.png"; include "/common/header.php"; ?>
        </div>

        <center>
            
            <a href="emsbreakfastmenu.php">
                <div class="download"/>
                    <h2>Breakfast Menu</h2>
                </div>
            </a>
            
            <a href="emslunchmenu.php">
                <div class="download"/>
                    <h2>Lunch Menu</h2>
                </div>
            </a>
           
            <a href="emsicecreammenu.php">
                <div class="download"/>
                    <h2>Ice Cream Menu</h2>
                </div>
            
            </a>

            <a href="files/krencysicecreammenu.pdf">
                <div class="download"/>
                    <h2>Download our Menu! PDF</h2>
                </div>
            </a>

        </center>


    </body>



</html>