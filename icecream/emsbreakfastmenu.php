<!DOCTYPE html>
<html>

    <head>

        <title>Krency's Ice Cream</title>
        <link rel="stylesheet" href="/css/content.css"/>
        <link rel="stylesheet" href="/css/icecream.css"/>
        <link rel="stylesheet" href="/css/header.css"/>

    </head>


    <body>

        <div class="header">
            <? $logo = "/images/bakery.png"; include "/common/header.php"; ?>
        </div>
        <center>

        <div class="row">

            <div class="foodgroup">
                <h2>Egg Breakfasts</h2>
                <table>
                    <tr>
                        <th>One Egg Breakfast</th>
                        <th>Price</th>
                    </tr>
                    <tr>
                        <td>with Toast</td>
                        <td>$2.59</td>
                    </tr>
                    <tr>
                        <td>with Homefries</td>
                        <td>$4.99</td>
                    </tr>
                    <tr>
                        <td>with Meat</td>
                        <td>$5.99</td>
                    </tr>
                    <tr>
                        <td>with Homefries and Meat</td>
                        <td>$7.99</td>
                    </tr>
                </table>

                <table>
                    <tr>
                        <th>Two Egg Breakfast</th>
                        <th>Price</th>
                    </tr>
                    <tr>
                        <td>with Toast</td>
                        <td>$3.09</td>
                    </tr>
                    <tr>
                        <td>with Homefries</td>
                        <td>$5.49</td>
                    </tr>
                    <tr>
                        <td>with Meat</td>
                        <td>$6.49</td>
                    </tr>
                    <tr>
                        <td>with Homefries and Meat</td>
                        <td>$8.49</td>
                    </tr>
                </table>

                <table>
                    <tr>
                        <th>Scrambled Eggs with Sausage and Hot Peppers</th>
                        <th>Price</th>
                    </tr>
                    <tr>
                        <td>with Toast</td>
                        <td>$5.99</td>
                </table>

            </div>

            <div class="foodgroup">
                <h2>Meals</h2>
                <table>
                    <tr>
                        <th>Italian Breakfast</td>
                        <th>$8.29</td>
                    </tr>
                    <tr>
                        <td> 2 Eggs scrambled with sausage, homefries, onion, tomato, hot pepper, and green pepper</td>
                    </tr>
                </table>
            </div>

        </div>

        <div class="row">



        </div>

        </center>

    </body>



</html>