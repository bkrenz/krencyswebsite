<!DOCTYPE html>
<?php
    include "/scripts/database/database.php";
    $db = new Database();

?>
<html>


    <head>
        
        <title>Krency's</title>
        <link rel="stylesheet" href="/css/frontpage.css"/>
        <link rel="stylesheet" href="/css/header.css"/>
        <meta name="keywords" content="cakes,cookies,desert,icecream,krencys,bakery,diner,weddings,events,food,breakfast,lunch,dinner" />

    </head>



    <body>

        <div class="header">
            <? $logo = "/images/bakery.png"; include "/common/header.php"; ?>
        </div>

        <center>

        <div class="locationbox">
            
            <center><h1>Krency&#8217s Bakery & Catering</h1></center>
           
            <img src="/images/bakery.png"/>
            
            <div class=separator>

                <div class="hours">
                    <center><h3>Hours</h3></center>
                    <ul>
                        <li>Sunday: Closed</li>
                        <li>Monday: Closed</li>
                        <li>Tuesday: 6am-5:30pm</li>
                        <li>Wednesday: 6am-5:30pm</li>
                        <li>Thursday: 6am-5:30pm</li>
                        <li>Friday: 6am-5:30pm</li>
                        <li>Saturday: 6am-5:30pm</li>
                    </ul>
                </div>

            </div>

            <div class=separator>
                
                <center>
                    <a href="bakery/bakery.php"/>
                        <div class="clickformore">
                            <h2>Click for menus and more!</h2>
                        </div>
                    </a>
                </center>

                <div class="contact">
                    <center><h3>Contact Info</h3></center>
                    <ul>
                        <li>(724) 222 - 6690</li>
                        <li>990 Jefferson Ave, Washington, PA 15301</li>
                    </ul>
                </div>

            </div>


        </div>

        
        <div class="locationbox">
            
            <center><h1>Krency&#8217s Ice Cream</h1><center>
               
            <img class="image" src="/images/icecream.png"/>

            <div class=separator>
                <div class="hours">
                    <center><h3>Hours</h3></center>
                    <table>
                        <tr>
                            <th>Day</th>
                            <th>Open</th>
                            <th>Close</th>
                        </tr>
                        <? echo $db -> formatAsTableRows( "SELECT * FROM emshours" , 3) ?>

                    </table>
                </div>
            </div>

            <div class=separator>   

                <center>
                    <a href="icecream/icecream.php"/>
                        <div class="clickformore">
                            <h2>Click for menus and more!</h2>
                        </div>
                    </a>
                </center>

                <div class="contact">
                    <center><h3>Contact Info</h3></center>
                    <ul>
                        <li>(724) 228 - 1640</li>
                        <li>690 East Maiden St, Washington, PA 15301</li>
                    </ul>
                </div>
            </div>
            

        </div>

        </center>

    </body>


</html>