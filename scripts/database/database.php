<?php

    class Database {

        // The database connection
        protected static $connection;

        /**
        * Try and connect to the database
        * @return bool false on failure, MySQL instance on success
        */
        public function connect() {

            // Try and connect to the database
            if ( !isset(self::$connection) ) {
                $config = parse_ini_file("config.ini.php");
                self::$connection = new mysqli($config["hostname"],$config["username"],$config["password"]);
                self::$connection -> select_db($config["dbname"]);
            }

            // Handle errors
            if ( self::$connection === false ) {
                return false;
            }

            return self::$connection;

        }


        /**
        * Query the database
        * @return Array of the rows of the query result
        */
        public function query($query)
        {
            // Get the connection
            $connection = $this -> connect();

            // Query the database
            $result = $connection -> query($query);
            if (!$result)
                return "(" . $connection->errno . ") " . $connection->error . "\n";

            // Setup the rows array
            $rows = array();
            while ( $row = $result -> fetch_array())
                $rows[] = $row;


            // Finish and return
            $result -> free();
            return $rows;
        }


        public function formatAsTableRows($query , $numRows) {

            // Get the table rows
            $rows = $this -> query( $query );
            
            $html = "";

            foreach ( $rows as $row )
            {
                $html .= "<tr>";
                for ( $i = 0 ; $i < $numRows ; $i++ )
                {
                    $html .= "<td>" . $row[$i] . "</td>";
                }
                $html .= "</tr>";
            }
            

            return $html;

        }

        public function testConnection()
        {
            $this -> connect();

            if ( self::$connection === false )
                return false;

            return true;
        }


    }









?>